from rest_framework import serializers
from api.models.tag import Tag


class TagSerializer(serializers.ModelSerializer):

    created_at = serializers.DateTimeField(
        format="%d/%m/%Y %H:%M:%S", read_only=True)
    count = serializers.SerializerMethodField()

    def get_count(self, obj):
        return obj.task_set.all().count()

    class Meta:
        model = Tag
        exclude = ('is_active',)


class TagNameSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tag
        exclude = ('id', 'is_active', 'created_at',)
