from rest_framework import serializers
from api.models.tasklist import TaskList


class TaskListSerializer(serializers.ModelSerializer):

    created_at = serializers.DateTimeField(
        format="%d/%m/%Y %H:%M:%S", read_only=True)

    class Meta:
        model = TaskList
        exclude = ('is_active',)
