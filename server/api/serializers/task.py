from rest_framework import serializers
from api.models.task import Task
from api.serializers.tag import TagNameSerializer


class TaskSerializer(serializers.ModelSerializer):

    created_at = serializers.DateTimeField(
        format='%d/%m/%Y %H:%M:%S', read_only=True)
    remind_me_on = serializers.DateField(format='%d/%m/%Y', required=False)
    tags = TagNameSerializer(many=True)

    class Meta:
        model = Task
        exclude = ('is_active',)

    def create(self, validated_data):
        return Task().create(**validated_data)
