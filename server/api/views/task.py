from rest_framework.exceptions import NotFound
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.pagination import PageNumberPagination
from api.models.task import Task
from api.serializers.task import TaskSerializer


class TaskAPIView(generics.ListCreateAPIView):
    description = 'This route is used to list or create a new task list.'
    queryset = Task.objects.filter(is_active=True).order_by('-created_at')
    pagination_class = PageNumberPagination
    serializer_class = TaskSerializer
    ordering = '-created_at'

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        serializer = TaskSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TaskDetail(APIView):
    """Get a Task List"""
    description = 'This route is used to get a single task.'

    def get_object(self, pk):
        try:
            return Task.objects.get(pk=pk, is_active=True)
        except Task.DoesNotExist:
            raise NotFound(detail='This task does not exist.', code=404)

    def get(self, request, pk):
        task = self.get_object(pk)
        serializer = TaskSerializer(task)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        task = self.get_object(pk)
        task.update(**request.data)
        serializer = TaskSerializer(task)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def delete(self, request, pk):
        task = self.get_object(pk)
        task.remove()
        return Response(status=status.HTTP_204_NO_CONTENT)
