from rest_framework.exceptions import NotFound
from rest_framework import generics, status
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.views import APIView
from api.models.tasklist import TaskList
from api.serializers.tasklist import TaskListSerializer


class TaskListAPIView(generics.ListCreateAPIView):
    description = 'This route is used to list or create a new task list.'
    queryset = TaskList.objects.filter(is_active=True).order_by('-created_at')
    pagination_class = PageNumberPagination
    serializer_class = TaskListSerializer
    ordering = '-created_at'

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        serializer = TaskListSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TaskListDetail(APIView):
    """Get a Task List"""
    description = 'This route is used to get a single task list.'

    def get_object(self, pk):
        try:
            return TaskList.objects.get(pk=pk, is_active=True)
        except TaskList.DoesNotExist:
            raise NotFound(detail='This task list does not exist.', code=404)

    def get(self, request, pk):
        tasklist = self.get_object(pk)
        serializer = TaskListSerializer(tasklist)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        tasklist = self.get_object(pk)
        tasklist.name = request.data.get('name', tasklist.name)
        tasklist.save()
        serializer = TaskListSerializer(tasklist)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def delete(self, request, pk):
        tasklist = self.get_object(pk)
        tasklist.is_active = False
        tasklist.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
