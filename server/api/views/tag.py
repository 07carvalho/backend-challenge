from rest_framework.exceptions import NotFound
from rest_framework import generics, status
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.views import APIView
from api.models.tag import Tag
from api.serializers.tag import TagSerializer


class TagAPIView(generics.ListCreateAPIView):
    description = 'This route is used to list or create a new tag.'
    queryset = Tag.objects.filter(is_active=True).order_by('-created_at')
    pagination_class = PageNumberPagination
    serializer_class = TagSerializer
    ordering = '-created_at'

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        serializer = TagSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TagDetail(APIView):
    """Get a Task List"""
    description = 'This route is used to get a single tag.'

    def get_object(self, pk):
        try:
            return Tag.objects.get(pk=pk, is_active=True)
        except Tag.DoesNotExist:
            raise NotFound(detail='This tag does not exist.', code=404)

    def get(self, request, pk):
        tag = self.get_object(pk)
        serializer = TagSerializer(tag)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        tag = self.get_object(pk)
        tag.name = request.data.get('name', tag.name)
        tag.save()
        serializer = TagSerializer(tag)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def delete(self, request, pk):
        tag = self.get_object(pk)
        tag.is_active = False
        tag.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
