from django.urls import path
from api.views.tracklist import TaskListAPIView, TaskListDetail
from api.views.tag import TagAPIView, TagDetail
from api.views.task import TaskAPIView, TaskDetail

urlpatterns = [
    path('taskList', TaskListAPIView.as_view(), name='tasklist_list'),
    path('taskList/<uuid:pk>',
         TaskListDetail.as_view(),
         name='tasklist_detail'),
    path('tags', TagAPIView.as_view(), name='tag_list'),
    path('tags/<uuid:pk>', TagDetail.as_view(), name='tag_detail'),
    path('tasks', TaskAPIView.as_view(), name='task_list'),
    path('tasks/<uuid:pk>', TaskDetail.as_view(), name='task_detail'),
]
