import uuid
from datetime import datetime
from django.db import models, transaction, IntegrityError
from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import ValidationError
from api.models.tasklist import TaskList
from api.models.tag import Tag


class Task(models.Model):
    """A Task"""
    class ActivityType(models.TextChoices):
        INDOORS = '1', _('Indoors')
        OUTDOORS = '2', _('Outdoors')

    class Status(models.TextChoices):
        OPEN = '1', _('Open')
        DONE = '2', _('Done')

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=400, blank=False, null=False)
    notes = models.CharField(max_length=1000, blank=False,
                             null=True, default=None)
    priority = models.IntegerField(blank=False, null=True, default=None)
    remind_me_on = models.DateField(blank=False, null=True, default=None)
    activity_type = models.CharField(max_length=1,
                                     choices=ActivityType.choices,
                                     null=True, default=None)
    status = models.CharField(max_length=1,
                              choices=Status.choices, default=Status.OPEN)
    task_list = models.ForeignKey(TaskList, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag, null=True, default=None)
    is_active = models.BooleanField(default=True, blank=False, null=False)
    created_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        app_label = 'api'

    def __str__(self):
        return '{0} - {1}'.format(self.id, self.title)

    def clear_tags(self):
        """remove all existing tags"""
        self.tags.clear()

    def update_tags(self, tags):
        self.clear_tags()
        for t in tags:
            if t.get('name', None):
                tag, created = Tag.objects.get_or_create(
                    name=t.get('name').lower())
                self.tags.add(tag)
            else:
                raise ValidationError(
                    detail={'tags': [{
                                'name': ['This field is required.']
                            }]}, code=400)


    def create(self, *args, **kwargs):
        try:
            with transaction.atomic():
                tags = kwargs.pop('tags', None)
                new_task = Task.objects.create(**kwargs)
                if tags is not None:
                    for t in tags:
                        # lower the tag name to keep consistency
                        tag, created = Tag.objects.get_or_create(
                            name=t.get('name').lower())
                        new_task.tags.add(tag)
                return new_task
        except IntegrityError:
            raise IntegrityError

    def update(self, *args, **kwargs):
        self.title = kwargs.get('title', self.title)
        self.notes = kwargs.get('notes', self.notes)
        self.priority = kwargs.get('priority', self.priority)

        if kwargs.get('remind_me_on') is not None:
            remind_me_on = datetime.strptime(
                kwargs.get('remind_me_on'), '%d/%m/%Y').date()
            self.remind_me_on = remind_me_on
        self.activity_type = kwargs.get('activity_type', self.activity_type)
        self.status = kwargs.get('status', self.status)
        self.task_list = kwargs.get('task_list', self.task_list)
        if kwargs.get('tags') and len(kwargs.get('tags')) > 0:
            self.update_tags(kwargs.get('tags'))
        self.save()

    def remove(self):
        self.is_active = False
        self.save()
