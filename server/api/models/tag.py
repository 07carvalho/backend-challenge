import uuid
from django.db import models


class Tag(models.Model):
    """A Tag"""
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=400, blank=False, null=False)
    is_active = models.BooleanField(default=True, blank=False, null=False)
    created_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        app_label = 'api'

    def __str__(self):
        return self.name
