from django.test import Client
from django.urls import reverse
from rest_framework.test import APITestCase
from api.models.task import Task
from api.models.tasklist import TaskList


class TestTagAPI(APITestCase):
    url = reverse('task_list')
    task_list_id = None

    def setUp(self) -> None:
        task_list = TaskList.objects.create(name='New Task List')
        self.task_list_id = str(task_list.id)

        for i in range(30):
            data = {
                'title': 'Task ' + str(i),
                'tags': [{'name': 'Tag'}, {'name': 'newTag'}],
                'task_list': task_list,
            }
            Task().create(**data)

    def test_list(self):
        """List the tasks"""
        response = Client().get(self.url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(10, len(response.data.get('results')))
        self.assertEqual(30, response.data.get('count'))
        self.assertFalse(None, response.data.get('next'))

    def test_post_creation(self):
        """Create a new task"""
        data = {
            'title': 'New Task',
            'tags': [{'name': 'Tag'}, {'name': 'newTag'}],
            'task_list': self.task_list_id,
        }
        response = Client().post(self.url, data, format='json',
                                 content_type='application/json')
        self.assertEqual(201, response.status_code)
        self.assertEqual(True, 'id' in response.data)
        self.assertEqual(True, 'title' in response.data)
        self.assertEqual(True, 'task_list' in response.data)
        self.assertEqual(True, 'tags' in response.data)
        self.assertEqual(None, response.data.get('notes'))
        self.assertEqual(None, response.data.get('priority'))
        self.assertEqual('1', response.data.get('status'))
        self.assertEqual(self.task_list_id,
                         str(response.data.get('task_list')))
        self.assertEqual(2, len(response.data.get('tags')))

    def test_post_without_title(self):
        """Try to create a new task without 'title' key"""
        data = {'description': 'Task 4'}
        response = Client().post(self.url, data)
        self.assertEqual(400, response.status_code)
        self.assertEqual(True, 'title' in response.data)


class TestTagDetailAPI(APITestCase):
    task_id = None

    def setUp(self) -> None:
        task_list = TaskList.objects.create(name='New Task List')
        data = {
            'title': 'Task 1',
            'tags': [{'name': 'Tag'}, {'name': 'newTag'}],
            'task_list': task_list,
        }
        obj = Task().create(**data)
        self.task_id = str(obj.id)

    def test_get_task(self):
        url = reverse('task_detail', kwargs={'pk': self.task_id})
        response = Client().get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual('Task 1', response.data.get('title'))

    def test_put_task(self):
        url = reverse('task_detail', kwargs={'pk': self.task_id})
        new_title = 'New Task'
        response = Client().put(url, {'title': new_title},
                                format='json', content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(new_title, response.data.get('title'))

    def test_update_remind_me_on_task(self):
        url = reverse('task_detail', kwargs={'pk': self.task_id})
        new_date = '20/01/2012'
        response = Client().put(url, {'remind_me_on': new_date},
                                format='json', content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(new_date, response.data.get('remind_me_on'))

    def test_delete_task(self):
        url = reverse('task_detail', kwargs={'pk': self.task_id})
        response = Client().delete(url)
        self.assertEqual(204, response.status_code)
        # request again
        response = Client().get(url)
        self.assertEqual(404, response.status_code)
