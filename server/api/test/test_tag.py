from django.test import Client
from django.urls import reverse
from rest_framework.test import APITestCase
from api.models.tag import Tag


class TestTagAPI(APITestCase):
    url = reverse('tag_list')

    def setUp(self) -> None:
        for i in range(30):
            data = {'name': 'Tag ' + str(i)}
            Tag.objects.create(**data)

    def test_list(self):
        """List the tags"""
        response = Client().get(self.url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(10, len(response.data.get('results')))
        self.assertEqual(30, response.data.get('count'))
        self.assertFalse(None, response.data.get('next'))

    def test_post_creation(self):
        """Create a new tag"""
        data = {'name': 'Tag 4'}
        response = Client().post(self.url, data)
        self.assertEqual(201, response.status_code)
        self.assertEqual(True, 'id' in response.data)
        self.assertEqual(True, 'created_at' in response.data)
        self.assertEqual(True, 'name' in response.data)
        self.assertEqual(True, 'count' in response.data)
        self.assertEqual(data.get('name'), response.data.get('name'))
        self.assertEqual(0, response.data.get('count'))

    def test_post_without_name(self):
        """Try to create a new tag without 'name' key"""
        data = {'description': 'Tag 4'}
        response = Client().post(self.url, data)
        self.assertEqual(400, response.status_code)
        self.assertEqual(True, 'name' in response.data)


class TestTagDetailAPI(APITestCase):
    created_uuid = None

    def setUp(self) -> None:
        tag = {'name': 'Tag 1'}
        obj = Tag.objects.create(**tag)
        self.created_uuid = str(obj.id)

    def test_get_tag(self):
        url = reverse('tag_detail', kwargs={'pk': self.created_uuid})
        response = Client().get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual('Tag 1', response.data.get('name'))

    def test_put_tag(self):
        url = reverse('tag_detail', kwargs={'pk': self.created_uuid})
        new_name = 'New Tag'
        response = Client().put(url, {'name': new_name},
                                format='json', content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(new_name, response.data.get('name'))

    def test_delete_tag(self):
        url = reverse('tag_detail', kwargs={'pk': self.created_uuid})
        response = Client().delete(url)
        self.assertEqual(204, response.status_code)
        # request again
        response = Client().get(url)
        self.assertEqual(404, response.status_code)
